all: subtraction

subtraction: build/library.o build/main_function.o
	gcc -o bin/subtraction build/library.o build/main_function.o

build/%.o: src/%.c
	gcc -c $< -o $@
