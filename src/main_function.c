#include <stdio.h>
#include <stdlib.h>

extern int Subtract(int a, int b);

int main(void){
	int a = 10, b = 5;
	int result = SubtractInteger(a, b);
	printf("%d - %d = %d\n", a, b, result);

	a = 5; b = 10;
	result = SubtractInteger(a, b);
	printf("%d - %d = %d\n", a, b, result);
}
